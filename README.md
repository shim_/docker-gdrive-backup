mount your stuff into /backup
set ONETAR to 1 if you want to backup /backup as whole otherwise folders will be stored individually
set ENCRYPTION_PASSWORD 
set COMPRESS to one of the listed: gzip lzma bzip2 none
set DATEDIR to create a yyyy-mm-dd folder for every backup
set PARENT to your gdrive backup folder id

run backup to backup
run purge <days> to remove old backups
run restore yyyy-mm-dd to restore into /backup
